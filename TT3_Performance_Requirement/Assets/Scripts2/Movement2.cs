using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float playerSpeed;
    public float jumpSpeed;

    private bool isJumping;
    private float move;
    private Rigidbody2D rb;
    private Animator anim;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>(); //getting rigidbody component attached to Player
        anim = GetComponent<Animator>();//get the Animator componment from the Player
    }

    // Update is called once per frame
    void Update()
    {
        move = Input.GetAxis("Horizontal");//  to get input from keyboard (variable becomes -1 when left arrow or A is pressed and +1 when right arrow or D is pressed)

        rb.velocity = new Vector2(move * playerSpeed, rb.velocity.y);// Applies velocity to rigidbody

        if (move < 0)//condition to change direction 
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }
        else if (move > 0)
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
        }

        if (Input.GetButtonDown("Jump") && !isJumping)//condition to make player jump by adding force using AddForce
        {
            rb.AddForce(new Vector2(rb.velocity.x, jumpSpeed));
            isJumping = true;
        }

        RunAnimations();//to run animations inside update
    }

    private void OnTriggerEnter2D(Collider2D other)//a method to collect the coins
    {
        if (other.gameObject.CompareTag("Coin"))//referencing gameObjects with Tag "Coins"
        {
            Destroy(other.gameObject);//destroys and removes gameObject with Tag "Coins" from the game
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            isJumping = false;
        }
    }

    void RunAnimations() //sets a method for the values of variables i made in animator
    {
        anim.SetFloat("Movement", Mathf.Abs(move));//for float movement animations set to greater than or less than 0.1
        anim.SetBool("isJumping", isJumping);//for bool jumping set to true or false
    }
}