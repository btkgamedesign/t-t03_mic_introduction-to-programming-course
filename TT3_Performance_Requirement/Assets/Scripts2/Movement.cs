using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{

    float moveInput;
    public Rigidbody2D rb;
    public float speed;
    public Transform pos;
    public float radius;
    public LayerMask groundLayers;
    public float jumpForce;
    public float heightCutter;
    bool grounded;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }


    // Start is called before the first frame update
    void Start()
    {

    }

    // FixedUpdate is used for Physics and can be called more than once
    void FixedUpdate()
    {
        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);
    }

    // Update is called once per frame and used to check inputs  
    void Update()
    {
        moveInput = Input.GetAxis("Horizontal");
        //moveInput = 1 if D is pressed and -1 if A is pressed and 0 if no key is pressed

        grounded = Physics2D.OverlapCircle(pos.position,radius,groundLayers);
        //will be true if the circle touches objects on Layers specified by LayerMask

        if (grounded && Input.GetKeyDown(KeyCode.Space))
        {
            rb.velocity = Vector2.up * jumpForce;
            //player jumps at same height  
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            if (rb.velocity.y > 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * heightCutter);
                //the y axis velocity is reduced and player jump height will be reduced
            }
        }
    }
}