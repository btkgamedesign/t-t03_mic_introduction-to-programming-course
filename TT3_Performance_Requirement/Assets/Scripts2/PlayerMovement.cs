using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement2D : MonoBehaviour
{
    public float playerSpeed;
    public float jumpSpeed;

    private bool isJumping;
    private float move;
    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>(); //getting rigidbody component attached to Player
    }

    // Update is called once per frame
    void Update()
    {
        move = Input.GetAxis("Horizontal");//  to get input from keyboard (variable becomes -1 when left arrow or A is pressed and +1 when right arrow or D is pressed)

        rb.velocity = new Vector2(move * playerSpeed, rb.velocity.y);// Applies velocity to rigidbody

        if (move < 0)//condition to change direction 
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }
        else if (move > 0)
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
        }

        if (Input.GetButtonDown("Jump") && !isJumping)//condition to make player jump by adding force using AddForce
        {
            rb.AddForce(new Vector2(rb.velocity.x, jumpSpeed));
            isJumping = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            isJumping = false;
        }
    }
}