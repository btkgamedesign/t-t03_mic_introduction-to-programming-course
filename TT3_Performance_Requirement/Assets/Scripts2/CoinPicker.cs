using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CoinPicker : MonoBehaviour
{

    private float Coin = 0;

    public TextMeshProUGUI TextCoin;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.tag == "Coin")//to chek if objectr colliding with player has a tag "coin"
        {
            Coin++;
            TextCoin.text = Coin.ToString();

            Destroy(other.gameObject);
        }
    }
}
