using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void PlayGame()// creating a function that will be called whenewver we hit the play button
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);//loads the next level in the queue of levels as listed in the build settings
    }

    public void Nextlevel()// creating a function that will be called whenewver we hit the play button
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);//loads the next level in the queue of levels as listed in the build settings
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
