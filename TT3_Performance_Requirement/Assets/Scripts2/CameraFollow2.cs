using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class CameraFollow2: MonoBehaviour
{


    private Transform playerTransform;

    public float offset;




    // Use this for initialization 
    void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        //current cameras position is stored in variable temp
        Vector3 temp = transform.position;

        //i set cameras position x to be equal to players position x
        temp.x = playerTransform.position.x;

        //i set cameras position y to be equal to players position y
      /*temp.y = playerTransform.position.y;*/

        //this adds the offset value to the camera x position
        temp.x += offset;

        //this adds the offset value to the camera y position
     /*temp.y += offset;*/

        //i set back cameras temp position to cameras current positioin
        transform.position = temp;
    }
}
